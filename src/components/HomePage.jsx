function HomePage() {
  return <div>
    <header>
      <h1>Site de vente de chaussures du turfu</h1>
    </header>
    <main>
      <sidebar>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro doloremque assumenda ut sapiente voluptatum adipisci cupiditate officia nostrum. Id voluptate odio, nulla sequi vero dolorum corporis! Velit in expedita quod!
      </sidebar>
      <section>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam nobis praesentium dolorum sed neque? Error maxime doloribus fugiat, quis aliquam voluptate et quae nihil unde ex repellendus, eveniet veritatis nesciunt?
      </section>
    </main>
    <footer>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ut, maxime! Tempore distinctio aperiam veniam maxime odio, reiciendis iste natus voluptatum repudiandae dolores voluptates vel perspiciatis architecto. Culpa tempore blanditiis illum?
    </footer>
  </div>;
}

export default HomePage;
