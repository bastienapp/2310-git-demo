function ProductItem({ name, description }) {
  return <li>
    <h3>{ name }</h3>
    <p>{ description }</p>
  </li>;
}

export default ProductItem;
