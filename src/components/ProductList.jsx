import ProductItem from "./ProductItem";

function ProductList() {
  return <ul>
    <ProductItem name="Nike Air Jordan" description="C'est cool" />
    <ProductItem name="Adidas" description="C'est un peu moins bien" />
    <ProductItem name="Asics" description="C'est bien quand même" />
  </ul>;
}

export default ProductList;
