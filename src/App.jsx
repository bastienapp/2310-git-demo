import "./App.css";
import HomePage from "./components/HomePage";
import ProductList from "./components/ProductList";

function App() {
  return (
    <>
      <HomePage />
      <ProductList />
    </>
  );
}

export default App;
